class Work < ActiveRecord::Base
    has_and_belongs_to_many :themes
    has_and_belongs_to_many :people

    def find_neighbors
        #Work.find_by_sql("select * from works join themes_works on themes_works.work_id=works.id join themes on themes.id=themes_works.theme_id where theme_id in (select theme_id from themes_works where work_id=#{self.id}) and work_id <> #{self.id}")
        Work.find_by_sql("
        select *, works.id as id, sum(themes_works.weight * mirror.weight) as bond from works join themes_works on themes_works.work_id=works.id join themes on themes.id=themes_works.theme_id join themes_works as mirror on mirror.theme_id=themes_works.theme_id where works.id <> #{self.id} and mirror.work_id=#{self.id} group by works.id order by bond DESC;
        ")
    end

    def find_common_themes other_id
        Work.find_by_sql("
        select *, themes_works.weight * mirror.weight as bond from works join themes_works on themes_works.work_id=works.id join themes on themes.id=themes_works.theme_id join themes_works as mirror on mirror.theme_id=themes_works.theme_id where works.id=#{self.id} and mirror.work_id=#{other_id} order by bond DESC;
        ")
    end
end
