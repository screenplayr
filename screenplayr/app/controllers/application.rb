# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  # Pick a unique cookie name to distinguish our session data from others'
  session :session_key => '_screenplayr_session_id'
  layout "main"

  def index
  end

  def search
    @query = params['query']
    @results = Work.find_by_sql("select * from works where title like '%#{@query}%'")
  end
end
