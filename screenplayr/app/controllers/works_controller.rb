class WorksController < ApplicationController
    layout "main"
    def index
        @id = params['id']
        @work = Work.find @id
        @neighbors = @work.find_neighbors
        @common_themes = {}
        @common_themes.default = []
        @neighbors.each do |neighbor|
            @common_themes[neighbor.id] = @work.find_common_themes neighbor.id
        end
    end

    def list
        @works = Work.find :all
    end
end
