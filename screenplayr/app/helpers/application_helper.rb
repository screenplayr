# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  WRAP_EXTENSION = "..." unless defined?(WRAP_EXTENSION)
  PATTERN = /(\S{30})/   unless defined?(PATTERN)
  REPLACEMENT = '\1 '    unless defined?(REPLACEMENT)

  def wrap(*args)
    case args.size #we accept only 1 or 2 args
    when 1
      @word = args[0]
      @wrap_length = 30 #default wrap
    when 2
      @word = args[0]
      @wrap_length = args[1] 
    else 
      raise ArgumentError, "This method takes either 1 or 2 arguments." 
    end 
    
    return "" if @word.nil? 
    
    #wrap!
    if @wrap_length != 0 && @word.length > @wrap_length
      @word = @word[0, (@wrap_length+1) - WRAP_EXTENSION.length] + WRAP_EXTENSION
    end
    
    #we replace a number of characters in a row with those characters plus a whitespace
    @word = @word.gsub(PATTERN, REPLACEMENT) #the multibyte support has trouble with ! methods
    @word
  end
end
