#!/bin/env ruby
require 'stop_words'
require 'mysql'

MIN_OCCURS = 2
db = Mysql.new(nil, 'root', 'fred23', 'screenplayr', nil, '/tmp/mysql.sock')

###############################################################################
create_work = db.prepare("insert ignore into works (title) values (?)")
create_theme = db.prepare("insert ignore into themes (name) values (?)")
create_theme_link = db.prepare("insert ignore into themes_works (work_id, theme_id, weight) values ((select id from works where title=?),(select id from themes where name=?), ?)")

ARGV.each {|file|
  puts "processing #{file}"
  themes = {}
  themes.default = 0
  File.open(file, 'r') do |f|
    f.each{|line|
      line.split.each {|word|
        if word =~ /^\w*$/ 
            word.downcase!
            themes[word] += 1 if word.length > 2 and !STOP_WORDS.include? word
        end
      }
    }
  end
  themes.each {|k,v|
    if v > MIN_OCCURS
      create_work.execute(file)
      create_theme.execute(k)
      create_theme_link.execute(file, k, v)
    end
  }
}

