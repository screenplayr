--create database screenplayr;
use screenplayr;
drop table if exists works;
drop table if exists themes;
drop table if exists people;
drop table if exists themes_works;
drop table if exists people_works;

create table works (id integer not null PRIMARY KEY auto_increment, title varchar(255) not null,
    unique title (title));
create table themes (id integer not null PRIMARY KEY auto_increment, name varchar(255) not null,
    unique name (name));
create table people (id integer not null PRIMARY KEY auto_increment, name varchar(255) not null, role enum('author','writer','actor','director'));

create table themes_works (work_id integer not null, 
    theme_id integer not null, 
    weight integer default 1, 
    foreign key (work_id) references works (id),
    foreign key (theme_id) references themes (id),
    unique link (work_id, theme_id)
);   

create table people_works (work_id integer not null, 
    person_id integer not null, 
    weight integer default 1, 
    foreign key (work_id) references works (id),
    foreign key (person_id) references people (id),
    unique link (work_id, person_id)
);   

-- TEST DATA
--insert into works (title) values ('Casablanca');
--insert into works (title) values ('A wonderful life');
--insert into works (title) values ('Citizen Kane');
--insert into works (title) values ('2001 Space Odyssey');
--
--insert into themes (name) values ('love');
--insert into themes (name) values ('white');
--insert into themes (name) values ('snow');
--
--insert into themes_works (work_id, theme_id, weight) values (2,1,10);
--insert into themes_works (work_id, theme_id, weight) values (2,2,14);
--insert into themes_works (work_id, theme_id, weight) values (1,1,20);
--insert into themes_works (work_id, theme_id, weight) values (1,2,32);
--insert into themes_works (work_id, theme_id, weight) values (1,3,2);
--insert into themes_works (work_id, theme_id, weight) values (3,2,4);

